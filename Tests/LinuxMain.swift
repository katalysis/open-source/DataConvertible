import XCTest
@testable import DataConvertibleTests

XCTMain([
    testCase(DataConvertibleTests.allTests),
])
